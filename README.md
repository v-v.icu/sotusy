# Product: Sotusy version 0.1.0 by v-v.icu

Files custom.css and index.html free for personal and commercial use under the CCA 4.0 license (http://creativecommons.org/licenses/by/4.0/)

## Credits:

Demo Images:
- To do (#)

Other:
- jQuery (jquery.com)
- Bootstrap (getbootstrap.com)

## V-V.icu contact & social networks
- v-v.icu  :: hello@v-v.icu
- LinkedIn :: https://www.linkedin.com/company/v-v-icu
- Twitter	 :: @v_v_icu :: https://twitter.com/v-v_icu
- Facebook :: https://www.facebook.com/vv.icu.face